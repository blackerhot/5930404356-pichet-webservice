var fs = require("fs");
var data = '';
var express = require('express');
var app = express();

var readerStream = fs.createReadStream('kaokonlakao.txt');

readerStream.setEncoding('UTF8');
readerStream.on('data', function (chunk) {
    data += chunk;
});

readerStream.on('end', function () {

    app.get('/', function (req, res) {

        res.send("<pre>" + data + "<pre>");
    });

});

readerStream.on('error', function (err) {
    console.log(err.stack);
});

console.log("Program Endded");

var server = app.listen(8081, function () {
    console.log("Example app listenting at http://127.0.0.1:8081");
});
