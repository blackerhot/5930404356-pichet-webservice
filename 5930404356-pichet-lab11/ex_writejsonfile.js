const jsonfile = require('jsonfile')
 
const file = 'data.json'
const obj = { name:"CM",
     courses:[
        "198330",
        "198371"
    ],
    places: {
        residence:"Khon Kaen",
        visites:[
            "Songkla",
            "Bangkok"
        ]
    }
}
 
var express = require('express');
var app = express();


jsonfile.writeFile(file, obj, function (err) {
  if (err) console.error(err)
})

jsonfile.readFile(file, function (err, obj) {
    if (err) console.error(err)
  
    console.log("=== The values of the second course and the residence ===")
    console.log("Studying "+obj.courses[1] + " living in "+ obj.places.residence)
    app.get('/', function (req, res) {
        
        res.send(obj);
    });
  })

  var server = app.listen(8081, function () {
    console.log("Example app listenting at http://127.0.0.1:8081");
});