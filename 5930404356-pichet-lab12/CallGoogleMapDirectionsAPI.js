var request1 = require("request");
var http = require('http');


request1.get("https://maps.googleapis.com/maps/api/directions/json?origin=Khon%20Kaen&destination=Bangkok&key=AIzaSyDI3k_kKwz0TSJPcqBV8OXTXG2gwJKaSOY", (error, response, body) => {
    if (error)
        return console.dir(error);
    var resob = JSON.parse(body);
    var obj = resob.routes[0].legs[0].steps;

    http.createServer(function (req, res) {
        res.writeHead(200, {
            "Content-Type": "text/html;charset=utf-8"
        });
        res.write("<h1>Directions from Khon Kaen to Bangkok</h1><ol>")
        for (var i = 0; i < obj.length; i++) {

            res.write('<li>' + obj[i].html_instructions+'</li>');
            res.write('<distance style="color:blue">('+ obj[i].distance.text+')</distance>');
        }
        res.end('</ol>');
    }).listen(8080);
});