'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PersonSchema = new Schema({
	country: {
		type: String,
		default:'Thailand'
	},
	name: {
		type: String,
		required: 'Kindly enter the name of the person'
	},
	weight: {
		type: Number,
		required: 'Kindly enter the name of the weigth'
	}
});

module.exports = mongoose.model('Persons', PersonSchema);

