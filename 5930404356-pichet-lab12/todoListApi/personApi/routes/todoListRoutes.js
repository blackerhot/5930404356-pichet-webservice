'user strict';
module.exports = function(app){
    var todoList =require('../controllers/todoListController');

    app.route('/persons')
        .get(todoList.list_all_persons)
        .post(todoList.create_a_persons);
    
    app.route('/persons/:personId')
        .get(todoList.read_a_persons)
        .put(todoList.update_a_persons)
        .delete(todoList.delete_a_persons);
    };